package com.rxcorp.rwi.interview

import org.apache.spark.sql.SparkSession

object Interview1 {
  /*
  1-) Read the sample.csv into spark environment.
  2-) This data is for a unique product and columns are: <shopId ,ignore ,month ,dayOfMonth ,price>
  Find the average price of the item per day.
  3-) Find the top 10 highest prices of the item during anytime and print them to stdout.
  4-) Save the average prices per day as csv file to an appropriate path on the local file system.
  5-) How would you test this job?
  6-) How would you verify the correctness of your results?
 */

  def main(args: Array[String]) {
    val csvFile = "src/main/resources/sample.csv"
    val spark = SparkSession.builder.appName("Interview").master("local[*]").getOrCreate()
    // Please write your code here.
    spark.stop()

  }

}